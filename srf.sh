#!/usr/bin/env bash

# ~/srf.sh

# Ask for the administrator password upfront.
sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Check for Homebrew,
# Install if we don't have it
if test ! $(which brew); then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" </dev/null
fi

# Make sure we’re using the latest Homebrew.
brew update

echo "------------------------------"
echo "Setting up SRF Settings."

echo "------------------------------"

###############################################################################
# Gradle			                                                                #
###############################################################################
echo "Copy Gradle Settings."
cp ./init/gradle.properties ~/.gradle/gradle.properties.test

echo "------------------------------"

###############################################################################
# Add hosts for SAP that it works also over VPN                               #
###############################################################################
hosts_file="/private/etc/hosts"
# Check if the host entry already exists
if ! grep -q "## SAP" "$hosts_file"; then
	echo "Adding SAP specific hosts to the hostfile $hosts_file"
  sudo sh -c "cat << EOF >> $hosts_file

## SAP
146.159.223.33 sapportal.es.srgssr.ch
146.159.223.34 portal.es.srgssr.ch
146.159.223.35 idp.es.srgssr.ch"
else
	echo "SAP hosts already exists, skipping entry creation"
fi

echo "------------------------------"
###############################################################################
# SRF Tools		                                                                #
###############################################################################
echo "Install SRF Tools."

brew install --cask --appdir="/Applications" camunda-modeler

brew install --cask --appdir="/Applications" citrix-workspace
brew install --cask --appdir="/Applications" tunnelblick
sudo cp ./init/srf-mpc-test-inte.tblk "/Library/Application Support/Tunnelblick/Users/${USER}/test-v5-with-route-2.tblk"

echo "------------------------------"
###############################################################################
# Kubernetes	                                                                #
###############################################################################
# asdf plugin-list-all | grep kubectl		# show repo
asdf plugin-add kubectl https://github.com/asdf-community/asdf-kubectl.git
# asdf list all kubectl		# show available versions
asdf install kubectl latest:1.24.17
asdf global kubectl 1.24.17

sudo mkdir /opt/kubectx
curl https://raw.githubusercontent.com/ahmetb/kubectx/master/kubectx -o ~/kubectx
sudo mv ~/kubectx /opt/kubectx/kubectx
curl https://raw.githubusercontent.com/ahmetb/kubectx/master/kubens -o ~/kubens
sudo mv ~/kubens /opt/kubectx/kubens

# Link Scripts
sudo ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens

# Berechtigung ändern
sudo chmod 755 /usr/local/bin/kubectx
sudo chmod 755 /usr/local/bin/kubens

echo "------------------------------"

echo "TODO: Install VPN config from ${PWD}/init/srf-mpc-test-v5-inte.tblk."
echo "TODO: Update ~/.gradle/gradle.properties with your credentials."
echo "TODO: Add kubernetes config in ~/.kube/config."
echo "Script completed."
