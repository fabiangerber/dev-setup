#!/usr/bin/env bash

# Install command-line tools using Homebrew.

# Ask for the administrator password upfront.
sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Check for Homebrew,
# Install if we don't have it
if test ! $(which brew); then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" </dev/null
fi

# Make sure we’re using the latest Homebrew.
brew update

# Office
#brew install --cask --appdir="/Applications" parallels
brew install --cask --appdir="/Applications" microsoft-office
brew install --cask --appdir="/Applications" microsoft-teams
brew install --cask --appdir="/Applications" microsoft-remote-desktop
#brew install --cask --appdir="/Applications" notion

# LaTeX
#brew install pygments             # required for minted
#brew install --cask mactex
#brew install --cask -appdir="/Applications" texstudio
# TODO: TexStudio -> Preferences -> Befehle -> PdfLaTeX
#     Add -shell-escape: pdflatex -synctex=1 -shell-escape -interaction=nonstopmode %.tex
#     see: https://tex.stackexchange.com/questions/48018/minted-not-working-on-mac

# Entertainment
brew install --cask --appdir="/Applications" calibre
#brew install --cask --appdir="/Applications" plex
brew install --cask --appdir="/Applications" vlc
brew install --cask --appdir="/Applications" spotify
brew install --cask --appdir="/Applications" whatsapp
brew install --cask --appdir="/Applications" telegram

# Drivers
brew tap homebrew/cask-drivers
brew install --cask --appdir="/Applications" sonos
brew install --cask --appdir="/Applications" garmin-basecamp
brew install --cask --appdir="/Applications" scroll-reverser
#brew install --cask --appdir="/Applications" nordvpn
brew install --cask --appdir="/Applications" hp-easy-start
brew install --cask --appdir="/Applications" rectangle
brew install --cask --appdir="/Applications" monitorcontrol

# Photography
#brew install --cask --appdir="/Applications" adobe-photoshop-lightroom
#brew install --cask --appdir="/Applications" adobe-creative-cloud
brew install --cask --appdir="/Applications" fastrawviewer
#brew install --cask --appdir="/Applications" qdslrdashboard

# Diverse
brew install --cask --appdir="/Applications" paragon-ntfs
#brew install --cask --appdir="/Applications" logitech-options
#brew install --cask --appdir="/Applications" little-snitch
#brew install --cask --appdir="/Applications" jdownloader
brew install --cask --appdir="/Applications" pdf-expert
#brew install youtube-dl

#brew install --cask --appdir="/Applications" teamviewer


# Remove outdated versions from the cellar.
brew cleanup


# Run Installers with manual Interactions
installer -pkg /usr/local/Caskroom/garmin-basecamp/4.7.0/Install\ BaseCamp.pkg -target /
#open /usr/local/Caskroom/adobe-creative-cloud/latest/Creative\ Cloud\ Installer.app/
open /usr/local/Caskroom/paragon-ntfs/15/FSInstaller.app

# Little Snitch -> Reboot required
#hdiutil attach /usr/local/Caskroom/little-snitch/4.2.2/LittleSnitch-4.2.2.dmg
#open /Volumes/Little\ Snitch\ 4.2.2/Little\ Snitch\ Installer.app/
# ->Reboot benötigt hdiutil detach /Volumes/Little\ Snitch\ 4.2.2/
