#!/usr/bin/env bash

# Install command-line tools using Homebrew.

# Ask for the administrator password upfront.
sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Check for Homebrew,
# Install if we don't have it
if test ! $(which brew); then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" </dev/null
fi

# Make sure we’re using the latest Homebrew.
brew update

brew tap homebrew/cask-versions
asdf plugin-add java
asdf install java temurin-21.0.4+7.0.LTS
asdf global java temurin-21.0.4+7.0.LTS

# asdf plugin-list-all | grep gradle		# show repo
asdf plugin add gradle https://github.com/rfrancis/asdf-gradle.git
# asdf list all gradle		# show available versions
asdf install gradle latest:8.8
asdf global gradle 8.8

#brew install maven
#brew install --ignore-dependencies gradle@7
#brew install android-sdk

#brew install --cask --appdir="/Applications" eclipse-java
#brew install --cask --appdir="/Applications" intellij-idea-ce
brew install --cask --appdir="/Applications" intellij-idea
brew install --cask --appdir="/Applications" datagrip
#brew install --cask --appdir="/Applications" android-studio
#brew install --cask --appdir="/Applications" noun-project
brew install --cask --appdir="/Applications" postman

#brew install cocoapods
# Remove outdated versions from the cellar.
brew cleanup
